#!/usr/bin/env bash

# This should be called from the application project root
echo '* * * * * Building Project * * * * *'
ng build --prod
#zip -r dist/fleet-track-web.zip dist
echo '* * * * * Zipping Package * * * * *'
cd dist
zip -r fleet-track-web.zip ./
cd ..
