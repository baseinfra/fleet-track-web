import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  currentPage = 'app-map';
  pageTitle = 'Dashboard';

  switchPage(page: string, title: string) {
    this.currentPage = page;
    this.pageTitle = title;
  }
}
