import {Vehicle} from '../model/vehicle.model';
import {Route} from '../model/route.model';

export class VehicleRouteDto {
  vehicle: Vehicle;
  route?: Route;
  tripId: string;
}
