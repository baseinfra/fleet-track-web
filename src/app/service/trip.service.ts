import {Injectable} from '@angular/core';
import {Trip} from '../model/trip.model';
import {RouteService} from './route.service';
import {VehicleService} from './vehicle.service';
import {Route} from '../model/route.model';
import {Vehicle} from '../model/vehicle.model';
import {Observable, of} from 'rxjs/index';
import index from '@angular/cli/lib/cli';

@Injectable()
export class TripService {
  routes: Route[] = [];
  vehicles: Vehicle[] = [];
  trips: Trip[] = [];

  constructor(private routeService: RouteService, private vehicleService: VehicleService) {
    this.routeService.getRoutes().subscribe(routes => this.routes = routes);
    this.vehicleService.getVehicles().subscribe(vehicles => this.vehicles = vehicles);
  }

  getTrips(): Observable<Trip[]> {
    return of(this.trips);
  }

  createTrips() {
    this.trips =
      [{id: 'tr1', route: this.routes[0], vehicle: this.vehicles[0], direction: 'forward', curRouteIndex: 0},
        {id: 'tr2', route: this.routes[1], vehicle: this.vehicles[1], direction: 'forward', curRouteIndex: 0}
      ];
    console.log(this.trips);
  }

  createTrip(r:Route, v:Vehicle) {
    let id = 'tr' + this.trips.length;
    this.trips.push({id: id,route: r, vehicle: v, direction: 'forward', curRouteIndex: 0})
  }

  removeTrip(tripId: string) {
    for (let i = 0; i < this.trips.length; i++) {
      let trip = this.trips[i];
      if (tripId === trip.id) {
        this.trips.splice(i, 1);
        break;
      }
    }
  }

  changeToNextLocation(tripId: string) {
    for (let trip of this.trips) {
      if (trip.direction == 'forward') {
        console.log('Moving to next location');
        trip.curRouteIndex += 1;
        if (trip.curRouteIndex >= trip.route.locations.length) {
          console.log('Reached the end of route. Will go back');
          trip.curRouteIndex -= 2;
          trip.direction = 'backward';
        }
      } else {
        console.log('Moving to next location');
        trip.curRouteIndex -= 1;
        if (trip.curRouteIndex < 0) {
          trip.curRouteIndex += 2;
          trip.direction = 'forward';
          console.log('Reached the start of route. Will go forward');
        }

      }
    }
  }

}
