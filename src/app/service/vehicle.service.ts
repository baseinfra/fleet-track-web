import {Injectable} from '@angular/core';
import {Vehicle} from '../model/vehicle.model';
import {VehicleStatus} from '../model/vehicle.status.model';
import {Observable, of} from 'rxjs/index';

@Injectable()
export class VehicleService {

  vehicles: Vehicle[] = [{
    id: 'cs_1',
    desc: 'V1',
    type: 'CAR',
    status: VehicleStatus.RUNNING,
    currentLoc: {lat: 10.310560, long: 123.896363}
  },
    {
      id: 'cs_2',
      desc: 'Toyota Vios [V2]',
      type: 'CAR',
      status: VehicleStatus.RUNNING,
      currentLoc: {lat: 10.308037, long: 123.893839}
    },
    {
      id: 'cs_3',
      desc: 'Toyota Fortuner [V3]',
      type: 'SUV',
      status: VehicleStatus.RUNNING,
      currentLoc: {lat: 10.309304, long: 123.892809}
    },
    {
      id: 'cs_4',
      desc: 'Mercedez Benz [V4]',
      type: 'BUS',
      status: VehicleStatus.RUNNING,
      currentLoc: {lat: 10.309304, long: 123.892809}
    },
    {
      id: 'cs_5',
      desc: 'Mercedez Benz [V5]',
      type: 'BUS',
      status: VehicleStatus.RUNNING,
      currentLoc: {lat: 10.309304, long: 123.892809}
    },
    {
      id: 'cs_6',
      desc: 'Mercedez Benz [V6]',
      type: 'BUS',
      status: VehicleStatus.RUNNING,
      currentLoc: {lat: 10.309304, long: 123.892809}
    },
    {
      id: 'cs_7',
      desc: 'Mercedez Benz [V7]',
      type: 'BUS',
      status: VehicleStatus.RUNNING,
      currentLoc: {lat: 10.309304, long: 123.892809}
    },
    {
      id: 'cs_8',
      desc: 'Toyota HiAce [V8]',
      type: 'VAN',
      status: VehicleStatus.RUNNING,
      currentLoc: {lat: 10.309304, long: 123.892809}
    },
    {
      id: 'cs_9',
      desc: 'Toyota HiAce [V9]',
      type: 'VAN',
      status: VehicleStatus.RUNNING,
      currentLoc: {lat: 10.309304, long: 123.892809}
    },
    {
      id: 'cs_10',
      desc: 'Toyota HiAce [V10]',
      type: 'VAN',
      status: VehicleStatus.RUNNING,
      currentLoc: {lat: 10.309304, long: 123.892809}
    }

  ];

  getVehicles(): Observable<Vehicle[]> {
    console.log('Loading Vehicles');
    return of(this.vehicles);
  }

}
