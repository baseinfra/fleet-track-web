import {Component, Injectable, Input, OnInit} from '@angular/core';
import {VehicleService} from '../service/vehicle.service';
import {Vehicle} from '../model/vehicle.model';
import {Route} from '../model/route.model';
import {RouteService} from '../service/route.service';
import {TripService} from '../service/trip.service';
import {Trip} from '../model/trip.model';
import {VehicleRouteDto} from '../dto/vehicle.route.dto';
import {NgbModal, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'ngbd-modal-content',
  template: `
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Select Route</h4>
        <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <select (change)="selectionClicked($event)">
          <option value="0">Select</option>
          <option [value]="r.id" *ngFor="let r of routes">{{ r.name }}</option>
        </select>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-primary" (click)="okClicked()">OK</button>
        <button type="button" class="btn btn-outline-dark" (click)="activeModal.close('Close click')">Close</button>
      </div>
      <div *ngIf="this.selectedRoute == null && this.okIsClicked" class="alert alert-danger">{{ dialogMessage }}</div>
    </div>
  `
})
export class NgbdModalContent {

  routes: Route[];
  selectedRoute: Route;
  dialogMessage: string;
  okIsClicked: boolean = false;
  vehicle: Vehicle;
  vehiclesComponent: VehiclesComponent;

  constructor(public activeModal: NgbActiveModal, private routeService: RouteService, private tripService: TripService) {
    routeService.getRoutes().subscribe(routes => this.routes = routes);
  }

  selectionClicked(event: any) {
    var i = event.target.options.selectedIndex;
    console.log('Selection Clicked ', i);
    if (i === 0) {
      this.selectedRoute = null;
    } else {
      this.selectedRoute = this.routes[i - 1];
      this.dialogMessage = '';
    }
  }

  okClicked() {
    this.okIsClicked = true;
    if (this.selectedRoute == null) {
      this.dialogMessage = 'You have not selected a route';
    } else {
      this.dialogMessage = '';
      this.activeModal.close('Close click');
      this.tripService.createTrip(this.selectedRoute, this.vehicle);
      this.vehiclesComponent.loadTrips();
      this.vehiclesComponent.loadVehicleTrip();
    }
  }
}

@Component({
  selector: 'app-vehicles',
  templateUrl: './vehicles.component.html',
  styleUrls: ['./vehicles.component.css']
})
export class VehiclesComponent implements OnInit {

  vehicles: Vehicle[];
  trips: Trip[];
  vehicleRoutesDto: VehicleRouteDto[] = [];

  constructor(private vhService: VehicleService, private tripService: TripService, private modalService: NgbModal) {
    vhService.getVehicles().subscribe(vehicles => this.vehicles = vehicles);
    this.loadTrips();
    this.loadVehicleTrip();
  }

  getTripAssigned(v: Vehicle): Trip {
    for (let t of this.trips) {
      if (t.vehicle.id == v.id) {
        return t;
      }
    }

    return null;
  }

  onEndTrip(vr: VehicleRouteDto) {
    console.log('Ending trip: ', vr);
    this.tripService.removeTrip(vr.tripId);
    this.loadTrips();
    this.loadVehicleTrip();
  }

  onDispatch(vr: VehicleRouteDto) {
    console.log('Dispatching vehicle: ', vr);
    const modalRef = this.modalService.open(NgbdModalContent);
    modalRef.componentInstance.vehicle = vr.vehicle;
    modalRef.componentInstance.vehiclesComponent = this;
  }

  loadTrips() {
    this.trips = [];
    this.tripService.getTrips().subscribe(t => this.trips = t);
  }

  loadVehicleTrip() {
    this.vehicleRoutesDto = [];
    this.vehicles.forEach(v => {
      let vRtDto = new VehicleRouteDto();
      vRtDto.vehicle = v;
      let trip = this.getTripAssigned(v);
      if (trip != null) {
        vRtDto.route = trip.route;
        vRtDto.tripId = trip.id;
      }

      this.vehicleRoutesDto.push(vRtDto);
    });

  }

  ngOnInit() {
  }

}
