import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {AgmCoreModule} from '@agm/core';

import {AppComponent} from './app.component';
import {MapComponent} from './map/map.component';
import {VehicleService} from './service/vehicle.service';
import {RouteService} from './service/route.service';
import {TripService} from './service/trip.service';
import {NgbdModalContent, VehiclesComponent} from './vehicles/vehicles.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    MapComponent,
    VehiclesComponent,
    NgbdModalContent
  ],
  imports: [
    BrowserModule,
    AgmCoreModule.forRoot({apiKey: 'AIzaSyB5WdGa7XNKDhUfimzS7ZjJgcvmrPpZA2M'}),
    FormsModule,
    NgbModule.forRoot()
  ],
  providers: [VehicleService, RouteService, TripService, VehiclesComponent
  ],
  bootstrap: [AppComponent],
  entryComponents: [NgbdModalContent]
})
export class AppModule {
}
