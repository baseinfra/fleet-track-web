import {Vehicle} from './vehicle.model';
import {Route} from './route.model';

/**
 * Will track also Trip state
 *
 */
export interface Trip {
  id: string;
  vehicle: Vehicle;
  route: Route;
  direction: string;
  curRouteIndex: number;  // tells the current index in the list of GLocation inside Route object

}
