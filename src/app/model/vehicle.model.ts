import {VehicleStatus} from './vehicle.status.model';
import {GLocation} from './glocation.model';


export interface Vehicle {
  id: string;
  desc: string;
  type: string;
  status: VehicleStatus;
  currentLoc: GLocation;

}
