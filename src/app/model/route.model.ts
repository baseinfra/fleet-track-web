import {GLocation} from './glocation.model';

export interface Route {
  id: string;
  name: string;
  locations: GLocation[];
}
