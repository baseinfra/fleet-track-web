export interface GLocation {
  lat: number;
  long: number;
}
