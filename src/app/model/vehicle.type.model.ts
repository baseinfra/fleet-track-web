export enum VehicleType {
  MOTORCYCLE,
  SEDAN,
  PICKUP,
  SMALL_VAN,
}
