import { Component } from '@angular/core';
import { MouseEvent } from '@agm/core';
import {VehicleService} from '../service/vehicle.service';
import {Vehicle} from '../model/vehicle.model';
import {TripService} from '../service/trip.service';
import {Trip} from '../model/trip.model';


@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent {

  constructor(private vehicleService: VehicleService, private tripService: TripService) {
    this.triggerTimeEvents();
  }
  // google maps zoom level
  zoom = 14;

  // initial center position for the map
  lat = 10.3080605;
  lng = 123.8935462;

  triggerTimeEvents() {
    setInterval(() => {console.log('Time triggered');
                                this.getTrips(); this.tripService.changeToNextLocation("1")},2000);
  }

  getTrips() {
    this.tripService.getTrips().subscribe(trips => this.trips = trips );
    console.log(this.trips);
  }

  trips: Trip[];

}
